package net.sswilliam.netty.test.protobuf_short_connect_client;

import static org.junit.Assert.*;

import java.util.concurrent.TimeoutException;

import net.sswilliam.netty.ProtobufShortConnectClient;

import org.junit.Test;

public class TestChortConnectClient {

	
	@Test
	public void test_no_server() {
		ProtobufShortConnectClient client = new ProtobufShortConnectClient();
		client.start();
		try {
			client.request("localhost", 8000, new byte[]{0,0,0,0});
		} catch (Exception e) {
			assertEquals("java.net.ConnectException: Connection refused: localhost/127.0.0.1:8000", e.getMessage());
			// TODO: handle exception
		}
	}
	
	@Test
	public void test_no_start_before_request() {
		ProtobufShortConnectClient client = new ProtobufShortConnectClient();
//		client.start();
		try {
			client.request("localhost", 1234, new byte[]{0,0,0,0});
		} catch (Exception e) {
			assertEquals("client is not started", e.getMessage());
			// TODO: handle exception
		}
	}
	@Test
	public void test_server_with_timeout(){
		ProtobufShortConnectClient client = new ProtobufShortConnectClient();
		client.start();
		try {
			client.request("localhost", 80, new byte[]{0,0,0,0});
		} catch (Exception e) {
			assertTrue(true);
			// TODO: handle exception
		}
	}

}
