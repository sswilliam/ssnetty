package net.sswilliam.netty;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import net.sswilliam.utils.DebugUtils;

public class ProtobufShortConnectClient {

	private ExecutorService exec;
	private boolean isStarted = false;
	public ProtobufShortConnectClient() {
		// TODO Auto-generated constructor stub
	}
	public void start(){
		start(true);
	}
	public void start(boolean isSync){
		if(isStarted){
			return;
		}
		isStarted = true;
		if(isSync){

			exec = Executors.newSingleThreadExecutor();
		}else{
			exec = Executors.newCachedThreadPool();
		}
	}
	public void stop(){
		if(!isStarted){
			return;
		}
		isStarted = false;
		exec.shutdown();
		
	}
	public boolean isStart(){
		return isStarted;
	}
	
	public ByteBuf request(String host, int port, ByteBuf command) throws Exception{
		if(!isStarted){
			throw new Exception("client is not started");
		}
		ProtobufShortConnectClientTask task = new ProtobufShortConnectClientTask(host, port, command);
		Future<ByteBuf> f = exec.submit(task);
		ByteBuf b = null;;
		try {
			b = f.get(10*DebugUtils.DEBUG_FRAME, TimeUnit.SECONDS);
		} catch (Exception e) {
			// TODO: handle exception
			f.cancel(true);
			throw e;
		} 
		
		return b;
	}
	public ByteBuf request(String host, int port, byte[] command) throws Exception{
		
		ByteBuf byteBuf = Unpooled.copiedBuffer(command);
		return request(host, port, byteBuf);
		
		
		
	}

}
