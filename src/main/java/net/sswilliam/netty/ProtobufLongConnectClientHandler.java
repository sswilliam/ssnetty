package net.sswilliam.netty;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class ProtobufLongConnectClientHandler extends SimpleChannelInboundHandler<ByteBuf>{

	private IProtobufLongConnectDataHandler datahandler;
	public ProtobufLongConnectClientHandler(IProtobufLongConnectDataHandler dataHandler){
		this.datahandler = dataHandler;
	}
	
	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		// TODO Auto-generated method stub
		super.channelActive(ctx);
		datahandler.onConnect(ctx.channel());
	}
	
	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		// TODO Auto-generated method stub
		super.channelInactive(ctx);
		datahandler.onDisconnect(ctx.channel());
	}
	
	@Override
	protected void messageReceived(ChannelHandlerContext ctx, ByteBuf msg)
			throws Exception {
		// TODO Auto-generated method stub
		datahandler.onData(ctx.channel(), msg);
		
		
	}
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
			throws Exception {
		// TODO Auto-generated method stub
		super.exceptionCaught(ctx, cause);
		datahandler.onExcpetion(ctx.channel(), cause);
	}
}
