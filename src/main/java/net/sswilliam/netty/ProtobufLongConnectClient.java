package net.sswilliam.netty;

import java.util.concurrent.TimeoutException;

//import net.sswilliam.log.Log;
import net.sswilliam.utils.sleeper.Sleeper;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

public class ProtobufLongConnectClient {

	private Channel targetChannel;
//	private String host;
//	private int port;
//	private IProtobufLongConnectDataHandler handler;
	private boolean shouldDisconnect = false;
	public ProtobufLongConnectClient(){
		
	}
	
	public boolean isConnected(){
		if(targetChannel == null){
			return false;
		}
		return targetChannel.isActive();
	}
	
	public Channel getTargetChannel(){
		return this.targetChannel;
	}
	
	public void connect(final String host, final int port, final IProtobufLongConnectDataHandler handler) throws Throwable{
		Throwable excpetion = null;
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if(targetChannel!= null){
					if(targetChannel.isActive()){
						return;
					}
				}
				shouldDisconnect = false;
				
				
				EventLoopGroup group = new NioEventLoopGroup();
				
		        try {
		        	 Bootstrap b = new Bootstrap();
		             b.group(group)
		              .channel(NioSocketChannel.class)
		              .handler(new ProtobufLongConnectHandlerInitializer(handler));

		             // Make a new connection.
		             Channel ch = b.connect(host, port).sync().channel();
		             targetChannel = ch;
//		             Log.Debug("connection loop start", this);
		             while(true){
		            	 	if(shouldDisconnect){
		            	 		break;
		            	 	}
		            	 	Sleeper.sleep(1000);
		             }

//		             Log.Debug("connection loop ends", this);
		             
		            // Get the handler instance to initiate the request.
		            
		            // Print the response at last but not least.
		           
		        } catch(Throwable e){ 
//		        		Log.Excption(e);
		        		
		        		
		        }finally {
		        		targetChannel = null;
		        		
		            group.shutdownGracefully();
		        }
			}
		}).start();
		long startWait = System.currentTimeMillis();
		while(true){
			if(excpetion != null){
				throw excpetion;
			}
			if(targetChannel != null){
				return;
			}
			if(System.currentTimeMillis() - startWait > 5000){
				throw new TimeoutException("connect to server timeout");
			}
			Sleeper.sleep(50);
		}
		
	}
//	public void reconnect() throws Throwable{
//		if(targetChannel != null){
//			if(targetChannel.isActive()){
//				targetChannel.close().sync();
//				targetChannel = null;
//			}
//		}
//		connect();
//		
//	}
	public void disconnect() throws Throwable{
		Channel channel = this.targetChannel;
		this.targetChannel = null;
		shouldDisconnect = true;
		if(channel != null){
			try {

				channel.close().sync();
				
			} catch (Exception e) {
//				Log.Excption(e);
				// TODO: handle exception
			} finally{

			}
		}
		
		
		
		
	}
	public void sendData(ByteBuf command) throws Exception{
		this.targetChannel.writeAndFlush(command).sync();
	}
	public void sendData(byte[] command) throws Exception{
		ByteBuf byteBuf = Unpooled.copiedBuffer(command);
		sendData(byteBuf);
	}
}
