package net.sswilliam.netty.handler;

import java.net.InetSocketAddress;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.ReferenceCountUtil;
//import net.sswilliam.log.Log;

public class CodeProtoBufHandler extends ChannelHandlerAdapter{
	
	private ICodeProtobufChannelHandler context;
	
	public CodeProtoBufHandler(ICodeProtobufChannelHandler context) {
		// TODO Auto-generated constructor stub
		this.context = context;
		System.out.println("handler inited");
	}
	
	
	@Override
	public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
		// TODO Auto-generated method stub
		super.channelRegistered(ctx);
		System.out.println("channel registerd");
	}
	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		// TODO Auto-generated method stub
		super.channelActive(ctx);
		
		
		
//		Log.Debug("channel active:"+getChannelInof(ctx.channel()), context);
		
	}
	
	
	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		// TODO Auto-generated method stub
		super.channelInactive(ctx);
		
//		Log.Debug("channel inactive:"+getChannelInof(ctx.channel()), context);
		context.onDisconnect(ctx.channel());
	}
	
	
	
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg)
			throws Exception {
		// TODO Auto-generated method stub
//		Log.Debug("channel read:"+getChannelInof(ctx.channel()), context);
		try {
			if(msg instanceof ByteBuf){
				
				ByteBuf byteMsg = (ByteBuf)msg;
//				System.out.println("++++++ "+byteMsg.readableBytes());
				byte code = byteMsg.getByte(0);
				byte[] body = new byte[byteMsg.readableBytes()-1];
				byteMsg.getBytes(1, body);
				
				context.submit(code, body, ctx.channel());
				
			}else{
//				Log.Warn("The msg is not a bytebug, it is a "+msg.getClass().toString(), context);
			}
		} catch (Exception e) {
			// TODO: handle exception
//			Log.Error("handling msg error"+e.getMessage(), context);
//			Log.Excption(e);
		}finally{
			ReferenceCountUtil.release(msg);
		}
	}
	public String getChannelInof(Channel ch){

		InetSocketAddress address = (InetSocketAddress)ch.remoteAddress();
		
		return "[ip:"+address.getAddress().getHostAddress()+", port:"+address.getPort()+"]";
//		return "No Info";
	}
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
			throws Exception {
		// TODO Auto-generated method stub
//		Log.Error("Exception caught in Distribute Server handler:"+cause.getMessage(), this);
//		Log.Excption(cause);
		context.onDisconnect(ctx.channel());
		ctx.close();
		
	}
	
}
