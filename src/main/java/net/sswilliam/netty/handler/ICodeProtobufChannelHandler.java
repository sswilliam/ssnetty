package net.sswilliam.netty.handler;

import io.netty.channel.Channel;

public interface ICodeProtobufChannelHandler {

	public void submit(byte code, byte[] body, Channel channel);
	public void onException(Channel channel);
	public void onDisconnect(Channel channel);
}
