package net.sswilliam.netty.util;

import io.netty.buffer.ByteBuf;

public class ByteBufUtil {

	
	public static byte[] getByteArray(ByteBuf buf){
		byte[] ret = new byte[buf.readableBytes()];
		buf.getBytes(0, ret);
		return ret;
	}

}
