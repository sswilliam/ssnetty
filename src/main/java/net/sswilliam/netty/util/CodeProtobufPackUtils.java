package net.sswilliam.netty.util;

import com.google.protobuf.GeneratedMessageLite;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

public class CodeProtobufPackUtils {

	public static class CodeProtobufPackPair{
		public byte code;
		public byte[] data;
	};
	public static ByteBuf pack(byte code, GeneratedMessageLite msg){
		ByteBuf buf = Unpooled.buffer();
		buf.writeByte(code);
		buf.writeBytes(msg.toByteArray());
		return buf;
	}
	
	public static CodeProtobufPackPair unpack(ByteBuf buf) throws Exception{
		int len = buf.readableBytes();
		if(len < 2){
			throw new Exception("The buf is too small to get a CodeProtobufPair "+len);
		}
		byte code = buf.readByte();
		byte[] data = new byte[len-1];
		buf.readBytes(data);
		CodeProtobufPackPair ret = new CodeProtobufPackPair();
		ret.code = code;
		ret.data = data;
		return ret;
		
	}
}
