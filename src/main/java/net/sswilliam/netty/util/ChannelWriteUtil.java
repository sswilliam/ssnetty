package net.sswilliam.netty.util;

import java.net.InetSocketAddress;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;

public class ChannelWriteUtil {

	public ChannelWriteUtil() {
		// TODO Auto-generated constructor stub
	}
	public static void write(Channel ch,byte code, byte[] data){
		ByteBuf buf = Unpooled.buffer(data.length+1);
		buf.writeByte(code);
		buf.writeBytes(data);
		ch.writeAndFlush(buf);
	}
	public static void write(Channel ch, byte[] data){
		ByteBuf buf = Unpooled.buffer(data.length);
		buf.writeBytes(data);
		ch.writeAndFlush(buf);
	}
	public static String getHost(Channel channel){
		InetSocketAddress address = (InetSocketAddress)channel.remoteAddress();
		return address.getAddress().getHostAddress();
	}
	public static String getPort(Channel channel){
		InetSocketAddress address = (InetSocketAddress)channel.remoteAddress();
		return address.getPort()+"";
		
	}

}
