package net.sswilliam.netty;

import java.net.SocketAddress;

import net.sswilliam.utils.DebugUtils;
import net.sswilliam.utils.sleeper.Sleeper;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.ReferenceCountUtil;

public class ProtobufShortConnectClientHandler extends SimpleChannelInboundHandler<ByteBuf> {

	
	private ByteBuf value = null;
	private Throwable cause = null;
	public ProtobufShortConnectClientHandler() {
		// TODO Auto-generated constructor stub
	}
	
	
	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		// TODO Auto-generated method stub
//		System.out.println("active");
		super.channelActive(ctx);
	}
	@Override
	public void write(ChannelHandlerContext ctx, Object msg,
			ChannelPromise promise) throws Exception {
		// TODO Auto-generated method stub
//		System.out.println("on write");
		super.write(ctx, msg, promise);
	}
	
	@Override
	public void bind(ChannelHandlerContext ctx, SocketAddress localAddress,
			ChannelPromise promise) throws Exception {
		// TODO Auto-generated method stub
//		System.out.println("on bind");
		super.bind(ctx, localAddress, promise);
	}
	
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg)
			throws Exception {
		// TODO Auto-generated method stub
//		System.out.println("on channel read");
		super.channelRead(ctx, msg);
	}
	
	
	


	
	@Override
	protected void messageReceived(ChannelHandlerContext ctx, ByteBuf msg)
			throws Exception {
		// TODO Auto-generated method stub
		try {

//			System.out.println("message received");
			value = msg.copy();
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}finally{
			ReferenceCountUtil.release(msg);
		}
		
	}
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
			throws Exception {
		// TODO Auto-generated method stub
		this.cause = cause;
	}
	
	public ByteBuf getProtoBufData() throws Throwable{
		
		int count = 0;
		while(true){
			if(value != null){
				return value;
			}
			if(cause != null){
				throw cause;
			}
			Sleeper.sleep(30);
			count++;
			if(count > 300*DebugUtils.DEBUG_FRAME){
				break;
			}
			
		}
		throw new Exception("Timeout, can't get data");
	}

}
