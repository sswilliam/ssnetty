package net.sswilliam.netty.server;

import net.sswilliam.utils.sleeper.Sleeper;


public class NettyServerTestUtil {

	public static void waitServerStart(NettyServer server, int time, int gap) throws Exception{
		int count = 0;
		while(true){
			if(count > time){
				throw new Exception("Server is not started in time");
			}
			
			if( server.isStarted()){
				return;
			}
			count++;
			Sleeper.sleep(gap);
		}
	}
	public static void waitServerStart(NettyServer server) throws Exception{
		waitServerStart(server,10,300);
	}
}
