package net.sswilliam.netty.server;

import net.sswilliam.netty.handler.ICodeProtobufChannelHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class NettyServer {

	private int port;
	EventLoopGroup bossGroup = new NioEventLoopGroup(1);
	EventLoopGroup workerGroup = new NioEventLoopGroup(2);
	private ICodeProtobufChannelHandler context;
	private Channel channel;
	

	public NettyServer(int port, ICodeProtobufChannelHandler context) {
		this.port = port;
		this.context = context;
	}

	public boolean isStarted(){
		return channel != null;
	}
	public void start() {
		if(channel != null){
			return;
		}
		try {
			ServerBootstrap b = new ServerBootstrap();
			b.group(bossGroup, workerGroup)
					.channel(NioServerSocketChannel.class)
					.childHandler(new NettyInitializer(this.context));

			this.channel = b.bind(port).sync().channel();
			this.channel.closeFuture().sync();

			
		} catch(Exception e){ 
			e.printStackTrace();
		}finally {
			bossGroup.shutdownGracefully();
			workerGroup.shutdownGracefully();
		}
	}

	public void stop() {
		if(channel == null){
			return;
		}
		channel.close();
		channel = null;
	}
}
