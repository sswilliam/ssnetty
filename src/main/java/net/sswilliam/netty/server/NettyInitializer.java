package net.sswilliam.netty.server;

import net.sswilliam.netty.handler.CodeProtoBufHandler;
import net.sswilliam.netty.handler.ICodeProtobufChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;

public class NettyInitializer extends ChannelInitializer<SocketChannel> {
	private ICodeProtobufChannelHandler context;
	public NettyInitializer(ICodeProtobufChannelHandler context){
		this.context = context;
	}
	protected void initChannel(SocketChannel ch) throws Exception {
//		System.out.println(">>>>>>>> start init channel");
		ChannelPipeline pipeline = ch.pipeline();
		pipeline.addLast("frameDecorder", new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4,0,4));
		pipeline.addLast("frameEncorder", new LengthFieldPrepender(4));
		CodeProtoBufHandler handler = new CodeProtoBufHandler(context);
		pipeline.addLast("handler",handler);
//		System.out.println(">>>>>> channel init finished");
	}

}
