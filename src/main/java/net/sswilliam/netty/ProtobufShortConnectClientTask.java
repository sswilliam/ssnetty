package net.sswilliam.netty;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.util.concurrent.Callable;


public class ProtobufShortConnectClientTask implements Callable<ByteBuf> {

	private String host;
	private int port;
	private ByteBuf command;

	public ProtobufShortConnectClientTask(String host, int port, ByteBuf command) {
		// TODO Auto-generated constructor stub
		this.host = host;
		this.port = port;
		this.command = command;
	}

	public ByteBuf call() throws Exception {
		// TODO Auto-generated method stub
		EventLoopGroup group = new NioEventLoopGroup(1);
		try {
			Bootstrap b = new Bootstrap();
			b.group(group).channel(NioSocketChannel.class)
					.handler(new ProtobufShortConnectHandlerInitializer());

			// Make a new connection.

			
			Channel ch = b.connect(host, port).sync().channel();
//			System.out.println("connected");
			// Get the handler instance to initiate the request.
			ProtobufShortConnectClientHandler handler = ch.pipeline().get(
					ProtobufShortConnectClientHandler.class);

//			System.out.println("start send data");
			ch.writeAndFlush(command);
//			System.out.println("data send finished");

			// Request and get the response.

			ByteBuf response = handler.getProtoBufData();

			try {
				ch.close();
			} catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
			// Close the connection.
			return response;

			// Print the response at last but not least.

		} catch (Throwable e) {
			e.printStackTrace();
			if (e instanceof Exception) {
				throw (Exception) e;
			} else {
				throw new Exception(e.getMessage());
			}
		} finally {

			group.shutdownGracefully();
		}
	}

}
