package net.sswilliam.netty;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;

public class ProtobufLongConnectHandlerInitializer extends ChannelInitializer<SocketChannel> {

	private IProtobufLongConnectDataHandler datahandler;
	public ProtobufLongConnectHandlerInitializer(IProtobufLongConnectDataHandler handler) {
		// TODO Auto-generated constructor stub
		this.datahandler = handler;
	}

	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		// TODO Auto-generated method stub
		ChannelPipeline pipeline = ch.pipeline();
		pipeline.addLast("frameDecorder", new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4,0, 4));
		pipeline.addLast("frameEncorder", new LengthFieldPrepender(4));
		
		pipeline.addLast("handler",new ProtobufLongConnectClientHandler(this.datahandler));
		
		
	}
}
