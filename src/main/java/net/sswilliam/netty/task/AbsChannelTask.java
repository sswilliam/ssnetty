package net.sswilliam.netty.task;

import io.netty.channel.Channel;

import java.util.concurrent.Callable;

public abstract class AbsChannelTask implements Callable<Object> {
	protected Channel targetChannel;
	protected byte code = -1;
	protected byte[] extraData = null;
	public AbsChannelTask(Channel channel, byte code, byte[] data) {
		// TODO Auto-generated constructor stub
		this.targetChannel = channel;
		this.code = code;
		this.extraData = data;
	}

	
}
