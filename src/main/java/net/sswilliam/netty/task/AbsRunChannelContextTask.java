package net.sswilliam.netty.task;

import io.netty.channel.Channel;
import net.sswilliam.ssmvc.design.ITaskMonitor;
import net.sswilliam.ssmvc.impl.Context;
import net.sswilliam.ssmvc.impl.RunContextTask;

public abstract class AbsRunChannelContextTask extends RunContextTask {

	protected Channel channel;
	protected byte code = -1;
	protected byte[] extraData = null;
	public AbsRunChannelContextTask(ITaskMonitor monitor, Context context, Channel channel, byte code, byte[] data) {
		super(monitor, context);
		this.channel = channel;
		this.code = code;
		this.extraData = data;
	}

	public AbsRunChannelContextTask(Context context, Channel channel, byte code, byte[] data) {
		this(null, context, channel, code, data);
	}

	

}
