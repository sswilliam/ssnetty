package net.sswilliam.netty.task;

import io.netty.channel.Channel;
import net.sswilliam.ssmvc.design.ITaskMonitor;
import net.sswilliam.ssmvc.impl.CallContextTask;
import net.sswilliam.ssmvc.impl.Context;

public abstract class AbsCallChannelContextTask extends CallContextTask {

	protected Channel channel;

	protected byte code = -1;
	protected byte[] extraData = null;
	public AbsCallChannelContextTask(ITaskMonitor monitor, Context context, Channel channel, byte code, byte[] data) {
		super(monitor, context);
		this.channel = channel;
		this.code = code;
		this.extraData = data;
	}

	public AbsCallChannelContextTask(Context context, Channel channel, byte code, byte[] data) {
		this(null, context, channel, code, data);
	}

	
}
