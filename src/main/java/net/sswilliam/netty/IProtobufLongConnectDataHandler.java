package net.sswilliam.netty;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;

public interface IProtobufLongConnectDataHandler {

	public void onData(Channel ch,ByteBuf data);
	public void onExcpetion(Channel ch,Throwable e);
	public void onConnect(Channel ch);
	public void onDisconnect(Channel ch);
	
	
}
